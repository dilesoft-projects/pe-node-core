// https://www.npmjs.com/package/mongoist
import fs from 'fs';

let db;
if (fs.existsSync(`${__dirname}/../../../config/db_config.json`)) {
  const config = require('../../../config/db_config');

  const mongojs = require('mongojs');
  const mongoist = require('mongoist');
  const mongoat = require('./mongoat');

  let userString = '';

  if (config.user && config.password) {
    userString = `${config.user}:${config.password}@`;
  }

  const connectionString = `mongodb://${userString}${config.host}:${config.port}/${config.database}`;

  // _config = {
  //   "host" : "",
  //   "user" : "",
  //   "password" : "",
  //   "database" : ""
  // }

  db = mongoat(connectionString);
}
export default db;
