import {
  updateCore,
} from './functions';

async function update() {
  await updateCore();
  process.exit();
}

update();
